package com.ey.iot.microservice.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;

@Entity
@DynamicUpdate(true)
@SelectBeforeUpdate
@Table(name = "BATCH_JOB")
public class Batch_Job implements Serializable {
	/**
	 * Batch_Job
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "Id", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int Id;
	@Column(name = "Job_Id", unique = true, nullable = false)
	private String Job_Id;
	@Column(name = "Job_Status", nullable = false)
	private String Job_Status;
	@Column(name = "Login_User", nullable = false)
	private String Login_User;
	@Column(name = "Job_Desc")
	private String Job_Desc;
	@Column(name = "Job_Remarks")
	private String Job_Remarks;
	@Column(name = "Job_Start_Time")
	private String Job_Start_Time;
	@Column(name = "Job_End_Time")
	private String Job_End_Time;
	@Column(name = "Job_Type")
	private String Job_Type;
	@Column(name = "R_Cre_Time", nullable = false)
	private String R_Cre_Time;
	@Column(name = "R_Mod_Time")
	private String R_Mod_Time;
	@Column(name = "Freefld1")
	private String Freefld1;
	@Column(name = "Freefld2")
	private String Freefld2;
	@Column(name = "Freefld3")
	private String Freefld3;

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getJob_Id() {
		return Job_Id;
	}

	public void setJob_Id(String job_Id) {
		Job_Id = job_Id;
	}

	public String getJob_Status() {
		return Job_Status;
	}

	public void setJob_Status(String job_Status) {
		Job_Status = job_Status;
	}

	public String getLogin_User() {
		return Login_User;
	}

	public void setLogin_User(String login_User) {
		Login_User = login_User;
	}

	public String getJob_Desc() {
		return Job_Desc;
	}

	public void setJob_Desc(String job_Desc) {
		Job_Desc = job_Desc;
	}

	public String getJob_Remarks() {
		return Job_Remarks;
	}

	public void setJob_Remarks(String job_Remarks) {
		Job_Remarks = job_Remarks;
	}

	public String getJob_Start_Time() {
		return Job_Start_Time;
	}

	public void setJob_Start_Time(String job_Start_Time) {
		Job_Start_Time = job_Start_Time;
	}

	public String getJob_End_Time() {
		return Job_End_Time;
	}

	public void setJob_End_Time(String job_End_Time) {
		Job_End_Time = job_End_Time;
	}

	public String getJob_Type() {
		return Job_Type;
	}

	public void setJob_Type(String job_Type) {
		Job_Type = job_Type;
	}

	public String getR_Cre_Time() {
		return R_Cre_Time;
	}

	public void setR_Cre_Time(String r_Cre_Time) {
		R_Cre_Time = r_Cre_Time;
	}

	public String getR_Mod_Time() {
		return R_Mod_Time;
	}

	public void setR_Mod_Time(String r_Mod_Time) {
		R_Mod_Time = r_Mod_Time;
	}

	public String getFreefld1() {
		return Freefld1;
	}

	public void setFreefld1(String freefld1) {
		Freefld1 = freefld1;
	}

	public String getFreefld2() {
		return Freefld2;
	}

	public void setFreefld2(String freefld2) {
		Freefld2 = freefld2;
	}

	public String getFreefld3() {
		return Freefld3;
	}

	public void setFreefld3(String freefld3) {
		Freefld3 = freefld3;
	}
}
