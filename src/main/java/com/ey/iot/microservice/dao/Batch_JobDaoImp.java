package com.ey.iot.microservice.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ey.iot.microservice.model.Batch_Job;

@Transactional
@Repository
public class Batch_JobDaoImp
  implements Batch_JobDao
{
  @PersistenceContext
  private EntityManager entityManager;
  
  @SuppressWarnings("unchecked")
public List<Batch_Job> getAllJobs()
  {
    Criteria criteria = ((Session)this.entityManager.unwrap(Session.class)).createCriteria(Batch_Job.class);
    
    criteria.addOrder(Order.desc("R_Cre_Time"));
    return criteria.list();
  }
  
  public Batch_Job getJobById(String id)
  {
    Batch_Job job;
    if (id != "")
    {
      Criteria criteria = ((Session)this.entityManager.unwrap(Session.class)).createCriteria(Batch_Job.class);
      criteria.addOrder(Order.desc("R_Cre_Time"));
      criteria.add(Restrictions.eq("Job_Id", id));
      job = (Batch_Job)criteria.list().get(0);
    }
    else
    {
      job = null;
    }
    return job;
  }
  
  public void addJob(Batch_Job job)
  {
    Session sess = (Session)this.entityManager.unwrap(Session.class);
    sess.save(job);
  }
  
  public void updateJob(Batch_Job job)
  {
    Session sess = (Session)this.entityManager.unwrap(Session.class);
    sess.update(job);
  }
  
  public void deleteJob(String JobId)
  {
    Session sess = (Session)this.entityManager.unwrap(Session.class);
    Batch_Job batch_Job = getJobById(JobId);
    sess.delete(batch_Job);
  }
  
  public boolean isJobExists(String JobId)
  {
    Batch_Job job = getJobById(JobId);
    if (job == null) {
      return false;
    }
    return true;
  }
}
