package com.ey.iot.microservice.dao;

import java.util.List;

import com.ey.iot.microservice.model.DeviceModel;

public abstract interface DeviceDao {
	public abstract List<DeviceModel> finddata(String paramString);

	public abstract void deletedata(String paramString);

	public abstract void updateallfirmware(String status);

	public abstract void saveOrUpdate(DeviceModel jymodel);

	public abstract List<DeviceModel> checkregistration(String input);

	public abstract List<DeviceModel> findall();

	public List<DeviceModel> searchDevice(String keyword);

	public abstract List<DeviceModel> getDeviceByGatewayId(String gateway);

	public abstract boolean DeviceExists(String paramString);

	public abstract void addDevice(DeviceModel paramDeviceModel);

	public abstract DeviceModel getDeviceByDeviceId(String deviceid);
}
