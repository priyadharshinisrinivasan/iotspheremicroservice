package com.ey.iot.microservice.dao;

import java.util.List;

import com.ey.iot.microservice.model.Batch_Job;


public abstract interface Batch_JobDao
{
  public abstract List<Batch_Job> getAllJobs();
  
  public abstract Batch_Job getJobById(String paramString);
  
  public abstract void addJob(Batch_Job paramBatch_Job);
  
  public abstract void updateJob(Batch_Job paramBatch_Job);
  
  public abstract void deleteJob(String paramString);
  
  public abstract boolean isJobExists(String paramString);
}