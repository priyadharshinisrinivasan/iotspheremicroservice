package com.ey.iot.microservice.dao;

import java.util.List;

import com.ey.iot.microservice.model.LoginModel;

public abstract interface EYDao
{  
  public abstract List<LoginModel> finddata(String paramString);

public abstract void saveOrUpdate(LoginModel loginmodel);

}
