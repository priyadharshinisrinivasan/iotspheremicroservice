package com.ey.iot.microservice.service;



import java.util.List;

import org.hibernate.annotations.DynamicUpdate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ey.iot.microservice.dao.EYDao;
import com.ey.iot.microservice.model.LoginModel;

@Service("eyService")
@DynamicUpdate(true)
@Transactional
public class EYServiceImpl
  implements EYService
{
  @Autowired
  private EYDao dao;
  
  public void savedata(LoginModel LoginModel)
  {
    this.dao.saveOrUpdate(LoginModel);
  }
  
  public List<LoginModel> finddata(String input)
  {
    return this.dao.finddata(input);
  }
  

  

}
