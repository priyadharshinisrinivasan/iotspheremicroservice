package com.ey.iot.microservice.service;

import java.util.List;

import com.ey.iot.microservice.model.DeviceModel;

public abstract interface DeviceService {
	public abstract void savedata(DeviceModel paramDeviceModel);

	public abstract void deletedata(String paramString);

	public abstract List<DeviceModel> finddata(String paramString);

	public abstract List<DeviceModel> findall();

	public abstract List<DeviceModel> searchDevices(String keyword);

	public abstract List<DeviceModel> getDeviceByGatewayId(String gateway);

	public abstract boolean addDevice(DeviceModel paramDeviceModel);

	public abstract boolean isDeviceExists(String paramString);

	public abstract void saveOrUpdate(DeviceModel device);
}
