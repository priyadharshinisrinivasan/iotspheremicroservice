package com.ey.iot.microservice.service;

import java.util.List;

import org.hibernate.annotations.DynamicUpdate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ey.iot.microservice.dao.DeviceDao;
import com.ey.iot.microservice.model.DeviceModel;

@Service("DeviceService")
@DynamicUpdate(true)
@Transactional
public class DeviceServiceImpl implements DeviceService {
	@Autowired
	private DeviceDao dao;

	public void savedata(DeviceModel DeviceModel) {
		this.dao.saveOrUpdate(DeviceModel);
	}

	public synchronized boolean addDevice(DeviceModel Device) {
		if (this.dao.DeviceExists(Device.getDevice_Id())) {
			return false;
		}
		this.dao.addDevice(Device);
		return true;
	}

	public List<DeviceModel> finddata(String input) {
		return this.dao.finddata(input);
	}

	public void deletedata(String ssn) {
		this.dao.deletedata(ssn);
	}

	@Override
	public List<DeviceModel> findall() {
		return this.dao.findall();
	}

	@Override
	public List<DeviceModel> searchDevices(String keyword) {
		return dao.searchDevice(keyword);
	}

	@Override
	public List<DeviceModel> getDeviceByGatewayId(String gateway) {
		return dao.getDeviceByGatewayId(gateway);
	}

	public boolean isDeviceExists(String DeviceId) {
		return this.dao.DeviceExists(DeviceId);
	}

	@Override
	public void saveOrUpdate(DeviceModel device) {
		this.dao.saveOrUpdate(device);
	}

}
