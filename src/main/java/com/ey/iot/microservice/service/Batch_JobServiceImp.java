/**
 * 
 */
package com.ey.iot.microservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ey.iot.microservice.dao.Batch_JobDao;
import com.ey.iot.microservice.model.Batch_Job;

/**
 * @author Shubham.Dwivedi
 *
 */
@Service
public class Batch_JobServiceImp
  implements Batch_JobService
{
  @Autowired
  Batch_JobDao dao;
  
  public List<Batch_Job> getAllJobs()
  {
    return this.dao.getAllJobs();
  }
  
  public Batch_Job getJobById(String id)
  {
    return this.dao.getJobById(id);
  }
  
  public void addJob(Batch_Job job)
  {
    this.dao.addJob(job);
  }
  
  public void updateJob(Batch_Job job)
  {
    this.dao.updateJob(job);
  }
  
  public void deleteJob(String JobId)
  {
    this.dao.deleteJob(JobId);
  }
  
  public boolean isJobExists(String JobId)
  {
    return isJobExists(JobId);
  }
}
