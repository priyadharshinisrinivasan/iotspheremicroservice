package com.ey.iot.microservice.service;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Service;
import org.json.simple.JSONObject;

import com.ey.iot.microservice.utility.AppProperty;
import com.google.gson.JsonSyntaxException;
import com.microsoft.azure.sdk.iot.service.Device;
import com.microsoft.azure.sdk.iot.service.RegistryManager;
import com.microsoft.azure.sdk.iot.service.devicetwin.DeviceTwin;
import com.microsoft.azure.sdk.iot.service.devicetwin.DeviceTwinDevice;
import com.microsoft.azure.sdk.iot.service.devicetwin.Pair;
import com.microsoft.azure.sdk.iot.service.exceptions.IotHubException;

@Service
public class DeviceManagement {

	public String connectionStringIoTHub;

	public DeviceManagement() {
		this.connectionStringIoTHub = (new AppProperty()).getPropertyValue("connectionStringIoTHub");
	}

	public boolean isDeviceRegisted(String deviceId) throws IOException {
		System.out.println("KEY: " + connectionStringIoTHub);
		RegistryManager registryManager = RegistryManager.createFromConnectionString(connectionStringIoTHub);
		Device device;
		try {
			device = registryManager.getDevice(deviceId);
			if (device.getDeviceId().equals(deviceId)) {
				return true;
			}
		} catch (JsonSyntaxException | IotHubException e) {
			// e.printStackTrace();
			return false;
		}

		return false;
	}

	@SuppressWarnings("unchecked")
	public JSONObject deviceRegistration(String deviceId, JSONObject tag) {
		JSONObject jsonObject = new JSONObject();
		try {
			RegistryManager registryManager = RegistryManager.createFromConnectionString(connectionStringIoTHub);
			Device device = Device.createFromId(deviceId, null, null);
			try {
				device = registryManager.addDevice(device);
			} catch (IotHubException iote) {
				// If the device already exists.
				try {
					device = registryManager.getDevice(deviceId);
				} catch (IotHubException iotf) {
					iotf.printStackTrace();
				}
			}
			if (jsonObject.containsKey("DeviceId")) {
				DeviceTwin twinClient;
				twinClient = DeviceTwin.createFromConnectionString(connectionStringIoTHub);
				DeviceTwinDevice dev = new DeviceTwinDevice(deviceId);
				twinClient.getTwin(dev);
				System.out.println("Old Device Twin " + dev);
				String currentTags = dev.tagsToString();
				System.out.println("Currnet Tags " + currentTags);
				Set<Pair> tags = new HashSet<Pair>();
				tags.add(new Pair("Volt2", "5.0V"));
				dev.setTags(tags);
				twinClient.updateTwin(dev);
				twinClient.getTwin(dev);
				System.out.println("New Device Twin " + device);
			}
			jsonObject.put("DeviceId", device.getDeviceId());
			jsonObject.put("DeviceKey", device.getPrimaryKey());
			System.out.println("Device Id: " + device.getDeviceId());
			System.out.println("Device key: " + device.getPrimaryKey());
		} catch (IOException | IllegalArgumentException | NoSuchAlgorithmException | IotHubException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonObject;
	}

	@SuppressWarnings("unchecked")
	public JSONObject showAllDevices() throws IOException {
		JSONObject jsonObject = new JSONObject();
		ArrayList<String> deviceIDs = new ArrayList<String>();
		RegistryManager registryManager = RegistryManager.createFromConnectionString(connectionStringIoTHub);
		try {
			ArrayList<Device> devices = registryManager.getDevices(Integer.MAX_VALUE);
			for (Device dev : devices) {
				deviceIDs.add(dev.getDeviceId().toString());
			}

		} catch (IotHubException e) {
			e.printStackTrace();
		}
		jsonObject.put("Devices", deviceIDs);
		return jsonObject;
	}

	@SuppressWarnings("unchecked")
	public JSONObject deleteDevice(String deviceId) throws IOException {

		JSONObject jsonObject = new JSONObject();
		RegistryManager registryManager = RegistryManager.createFromConnectionString(connectionStringIoTHub);
		if (isDeviceRegisted(deviceId)) {
			try {
				registryManager.removeDevice(deviceId);
				jsonObject.put("Status", "Removed");
			} catch (IotHubException e) {
				jsonObject.put("Status", "Device Does not exist!");
			}

		} else
			jsonObject.put("Status", "Device Does not exist!");
		return jsonObject;
	}
}
