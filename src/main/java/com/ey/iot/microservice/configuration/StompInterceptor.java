package com.ey.iot.microservice.configuration;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptorAdapter;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.messaging.support.MessageHeaderAccessor;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureException;

public class StompInterceptor extends ChannelInterceptorAdapter {
	
	private final Logger logger = Logger.getLogger(this.getClass());
	private MessageChannel clientOutboundChannel;
	public StompInterceptor(MessageChannel clientOutboundChannel) {
		this.clientOutboundChannel = clientOutboundChannel;
	}
	
	private boolean tokencheck(String authHeader) {
		if (authHeader == null || !authHeader.startsWith("Bearer ")) {
			return false;
		}

		final String token = authHeader.substring(7);

		try {
			@SuppressWarnings("unused")
			final Claims claims = Jwts.parser().setSigningKey("secretkey").parseClaimsJws(token).getBody();
			return true;
		} catch (final SignatureException e) {
			return false;
		}

	}
	
	@Override
	public Message<?> preSend(Message<?> message, MessageChannel channel) {
		StompHeaderAccessor accessor = MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);
		if (StompCommand.CONNECT.equals(accessor.getCommand())) {
			List<String> header = accessor.getNativeHeader("Authorization");
			if (tokencheck(header.get(0))) {
				logger.info("Info: Header Matched");
				return message;
			} else {
				StompHeaderAccessor new_accessor = StompHeaderAccessor.create(StompCommand.ERROR);
				new_accessor.setMessage("Invalid Token");
				new_accessor.setSessionId(accessor.getSessionId());
				new_accessor.setSubscriptionId(accessor.getSubscriptionId());
				this.clientOutboundChannel.send(MessageBuilder.createMessage(new byte[0], new_accessor.getMessageHeaders()));
				// (new MessagingTemplate()).send(MessageBuilder.withPayload(message.getPayload()).setHeaders(new_accessor).build());
				logger.error("Error: Header did not match.");
				return null;
			}
		}
		return message;
	}
}