package com.ey.iot.microservice.websocket;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;

import com.ey.iot.microservice.model.DeviceMessage;

import java.lang.reflect.Type;

public class MyStompSessionHandler extends StompSessionHandlerAdapter {

    private Logger logger = Logger.getLogger(this.getClass());

    @Override
    public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
        logger.info("New session established : " + session.getSessionId());
        session.subscribe("/channel/public", this);
        logger.info("Subscribed to /channel/public");
        session.send("/app/chat.addDevice", getGreetingMessage());
        logger.info("Message sent to websocket server");
    }

    @Override
    public void handleException(StompSession session, StompCommand command, StompHeaders headers, byte[] payload, Throwable exception) {
        logger.error("Got an exception", exception);
    }

    @Override
    public Type getPayloadType(StompHeaders headers) {
        return DeviceMessage.class;
    }

    @Override
    public void handleFrame(StompHeaders headers, Object payload) {
    	//DeviceMessage msg = (DeviceMessage) payload;
        //logger.info("Received : " + msg.getContent().toJSONString());
    }

    private DeviceMessage getGreetingMessage() {
    	DeviceMessage msg = new DeviceMessage();
        msg.setSender("Microservice_Websocket_Client");
        msg.setType(DeviceMessage.MessageType.JOIN);
        msg.setContent(new JSONObject());
        return msg;
    }
}