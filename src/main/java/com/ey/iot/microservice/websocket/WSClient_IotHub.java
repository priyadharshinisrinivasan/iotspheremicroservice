package com.ey.iot.microservice.websocket;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.socket.WebSocketHttpHeaders;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.Transport;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;
import org.springframework.web.socket.sockjs.frame.Jackson2SockJsMessageCodec;

import com.ey.iot.microservice.utility.AppProperty;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class WSClient_IotHub {
	
	/*
	 * This class is connecting to local websocket. Through this class, IoTHud data
	 * is being passed to the Local websocket server then to IoTSphere CG
	 */

	private StompSession stompSession;
	// private Logger logger = Logger.getLogger(this.getClass());
	private String url;
	public String websocket_uri;
	public String microservice;
	private String token;
	private final static WebSocketHttpHeaders headers = new WebSocketHttpHeaders();

	public WSClient_IotHub() {
		setUrl((new AppProperty()).getPropertyValue("websocket_uri"));
		microservice = (new AppProperty()).getPropertyValue("microservice");
		setToken(
				"eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJtaWMuc2VydkBleS5jb20iLCJyb2xlcyI6InVzZXIiLCJpYXQiOjE1MTY2MjY2Njd9.aLufagzdFM6QOhhvmahHM1aiL4HvIstyetJ9YPNyftI");
	}

	public ListenableFuture<StompSession> connect() {
		Transport webSocketTransport = new WebSocketTransport(new StandardWebSocketClient());
		List<Transport> transports = Collections.singletonList(webSocketTransport);
		SockJsClient sockJsClient = new SockJsClient(transports);
		sockJsClient.setMessageCodec(new Jackson2SockJsMessageCodec());
		WebSocketStompClient stompClient = new WebSocketStompClient(sockJsClient);
		headers.add("Authorization", "Bearer " + getToken());
		return stompClient.connect(this.getUrl(), headers, new MyHandler());
	}

	public void subscribeGreetings(StompSession stompSession) {
		stompSession.subscribe("/channel/public", new StompFrameHandler() {
			public Type getPayloadType(StompHeaders stompHeaders) {
				return byte[].class;
			}

			public void handleFrame(StompHeaders stompHeaders, Object o) {
				// logger.info("Received greeting " + new String((byte[]) o));
			}
		});
	}

	public void sendDeviceData(StompSession stompSession, String string) {
		String jsonHello = "{ \"sender\" : \"MicroService\",\"type\" : \"CHAT\",\"content\" : " + string + "}";
		stompSession.send("/app/chat.sendMessage", jsonHello.getBytes());
	}

	private class MyHandler extends StompSessionHandlerAdapter {
		public void afterConnected(StompSession stompSession, StompHeaders stompHeaders) {
			// logger.info("Local Client Connected to :Local Server");
			String jsonHello = "{ \"sender\" : \"MicroService\",\"content\":{},\"type\" : \"JOIN\" }";
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			stompSession.send("/app/chat.addDevice", jsonHello.getBytes());
		}
	}

	public void setup() {

		boolean t;
		do {
			if (getToken() == null) {
				authToken();
				t = true;
			} else {
				try {
					// logger.info("Connecting to local websocket With token: "+getToken());
					ListenableFuture<StompSession> f = connect();
					if (f.get() == null) {
						// logger.info("Websocket Session is null, waiting for connection");
					} else {
						// logger.info("Session is not null, Connection done");
					}

					if (f.isDone()) {
						setStompSession(f.get());
						t = false;
					} else
						t = true;
					// break;
				} catch (ExecutionException | InterruptedException e) {
					// logger.info("Error while connecting... Trying again!!!");
					t = true;
				}
			}
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		} while (t);
		// logger.info("Subscribing to greeting topic using session " +
		// getStompSession().getSessionId());
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		subscribeGreetings(getStompSession());
	}

	private void authToken() {
		CloseableHttpClient client = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(microservice + "/authenticate");
		try {
			String userCredentials = "mic.serv@ey.com:iot";
			String basicAuth = "Basic " + new String(Base64.encode(userCredentials.getBytes()));
			StringEntity params = new StringEntity("dummy", "UTF-8");
			httpPost.setEntity(params);
			httpPost.setHeader("Authorization", basicAuth);
			CloseableHttpResponse response = client.execute(httpPost);
			if (response.getStatusLine().getStatusCode() == 200) {
				BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
				String readLine;
				String responseBody = "";
				while (((readLine = br.readLine()) != null)) {
					responseBody += readLine;
				}
				br.close();
				if (!(responseBody.equalsIgnoreCase("HTTP Status 401-Forbidden")))
					setToken(responseBody);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public StompSession getStompSession() {
		return stompSession;
	}

	public void setStompSession(StompSession stompSession) {
		this.stompSession = stompSession;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
