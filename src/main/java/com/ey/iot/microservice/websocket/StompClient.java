package com.ey.iot.microservice.websocket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.ExecutionException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Logger;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandler;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.socket.WebSocketHttpHeaders;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import com.ey.iot.microservice.utility.AppProperty;
import com.ey.iot.microservice.utility.MessageRead_IotHub;

public class StompClient {

	public StompSession stompSession = null;
	public String websocket_uri;
	public String microservice_auth;
	public String token = null;
	private static  Logger logger = Logger.getLogger(StompClient.class);
	public StompClient() {
		this.websocket_uri = (new AppProperty()).getPropertyValue("websocket_uri");
		this.microservice_auth = (new AppProperty()).getPropertyValue("microservice");
		this.token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJtaWMuc2VydkBleS5jb20iLCJyb2xlcyI6InVzZXIiLCJpYXQiOjE1MTY2MjY2Njd9.aLufagzdFM6QOhhvmahHM1aiL4HvIstyetJ9YPNyftI";
	}

	private void connect() {
		WebSocketClient client = new StandardWebSocketClient();
		WebSocketStompClient stompClient = new WebSocketStompClient(client);
		stompClient.setMessageConverter(new MappingJackson2MessageConverter());
		StompSessionHandler sessionHandler = new MyStompSessionHandler();
		StompHeaders headers = new StompHeaders();
		headers.add("Authorization", "Bearer " + this.token);
		 logger.info("before websocket-uri connect,token:"+this.token);
		ListenableFuture<StompSession> f = stompClient.connect(this.websocket_uri, new WebSocketHttpHeaders(), headers,
				sessionHandler);
		try {
			this.stompSession = f.get();
			 logger.info("Success stompsession fetch!");
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
			 logger.info("Failure stompsession fetch!"+e);
		}
	}

	public void setup() {
		boolean retry;
		do {
			if (this.token == null) {
				authToken();
				retry = true;
			} else {
				this.connect();
				retry = false;
			}
		} while (retry);
	}

	public void sendDeviceData(StompSession stompSession, String string) {
		String jsonHello = "{ \"sender\" : \"MicroService\",\"type\" : \"CHAT\",\"content\" : " + string + "}";
		stompSession.send("/app/chat.sendMessage", jsonHello.getBytes());
	}

	private void authToken() {
		CloseableHttpClient client = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(this.microservice_auth + "/authenticate");
		try {
			String userCredentials = "mic.serv@ey.com:iot";
			String basicAuth = "Basic " + new String(Base64.encode(userCredentials.getBytes()));
			StringEntity params = new StringEntity("dummy", "UTF-8");
			httpPost.setEntity(params);
			httpPost.setHeader("Authorization", basicAuth);
			CloseableHttpResponse response = client.execute(httpPost);
			if (response.getStatusLine().getStatusCode() == 200) {
				logger.info("Succesful /authenticate token fetch");
				BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
				String readLine;
				String responseBody = "";
				while (((readLine = br.readLine()) != null)) {
					responseBody += readLine;
				}
				br.close();
				if (!(responseBody.equalsIgnoreCase("HTTP Status 401-Forbidden")))
					this.token = responseBody;
			}
		} catch (IOException e) {
			 logger.info("Error in /authenticcate auth token!"+e);
			e.printStackTrace();
		}
	}
}
