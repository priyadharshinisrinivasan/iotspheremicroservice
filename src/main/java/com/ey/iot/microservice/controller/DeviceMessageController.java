package com.ey.iot.microservice.controller;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Controller;
import com.ey.iot.microservice.model.DeviceMessage;
import com.ey.iot.microservice.utility.MessageRead_IotHub;

@Controller
public class DeviceMessageController {
	MessageRead_IotHub messageRead = null;
	//private Logger logger = Logger.getLogger(this.getClass());
	@MessageMapping("/chat.sendMessage")
	@SendTo("/channel/public")
	public DeviceMessage sendMessage(@Payload DeviceMessage deviceMessage) {
		//logger.info(deviceMessage);
		return deviceMessage;
	}

	@MessageMapping("/chat.addDevice")
	//@SendTo("/channel/public")
	public DeviceMessage addUser(@Payload DeviceMessage deviceMessage, SimpMessageHeaderAccessor headerAccessor) {
		//logger.info("New Client "+deviceMessage.getSender()+" Connected.");
		headerAccessor.getSessionAttributes().put("deviceId", deviceMessage.getSender());
		return deviceMessage;
	}
}
