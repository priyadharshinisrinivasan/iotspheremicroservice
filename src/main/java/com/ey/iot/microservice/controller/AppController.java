package com.ey.iot.microservice.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.websocket.Session;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

import com.ey.iot.microservice.encrypt.BCryptutil;
import com.ey.iot.microservice.model.Batch_Job;
import com.ey.iot.microservice.model.DeviceModel;
import com.ey.iot.microservice.model.GatewayModel;
import com.ey.iot.microservice.model.LoginModel;
import com.ey.iot.microservice.service.Batch_JobService;
import com.ey.iot.microservice.service.DeviceManagement;
import com.ey.iot.microservice.service.DeviceService;
import com.ey.iot.microservice.service.EYService;
import com.ey.iot.microservice.service.GatewayService;
import com.ey.iot.microservice.utility.AppProperty;
import com.ey.iot.microservice.utility.JobConfiguration;
import com.ey.iot.microservice.utility.MessageRead_IotHub;
import com.microsoft.azure.eventhubs.EventHubException;
import com.microsoft.azure.sdk.iot.service.devicetwin.DeviceTwin;
import com.microsoft.azure.sdk.iot.service.devicetwin.DeviceTwinDevice;
import com.microsoft.azure.sdk.iot.service.devicetwin.Pair;
import com.microsoft.azure.sdk.iot.service.exceptions.IotHubException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;

@RestController
@RequestMapping("/")
public class AppController {
	
	public Logger logger = Logger.getLogger(this.getClass());
	public Session session;
	@Autowired
	MessageRead_IotHub messageRead;
	@Autowired
	Batch_JobService jobService; // Batch Job Database Service
	@Autowired
	GatewayService gatewayService; // Gateway Database service
	@Autowired
	DeviceService deviceService; // Device database service
	@Autowired
	DeviceManagement deviceManagement;
	@Autowired
	EYService eyservice;
	
	@SuppressWarnings("null")
	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public @ResponseBody String postauthenticate(final ModelMap model, HttpServletRequest request,
			UriComponentsBuilder ucBuilder) throws IOException {
		final String authHeader = request.getHeader("authorization");
		String emailuser = null;
		String password = null;
		try {
			if (authHeader != null || authHeader.startsWith("Basic ")) {

				String credentials = authHeader.substring(6);
				Base64 base64 = new Base64();
				String decoded = new String(base64.decode(credentials.getBytes()));
				String parts[] = decoded.split(":");
				emailuser = parts[0];
				password = parts[1];
			} else {
				return "failure";
			}

			List<LoginModel> jymodel = this.eyservice.finddata(emailuser);
			String computedhash = jymodel.get(0).getPwd();
			boolean flg = BCryptutil.verifypwd(password, computedhash);
			if (flg == true) {
				String jwtToken = "";
				jwtToken = Jwts.builder().setSubject(emailuser).claim("roles", "user").setIssuedAt(new Date())
						.signWith(SignatureAlgorithm.HS256, "secretkey").compact();
				return jwtToken;
			} else {
				return "failure";
			}
		} catch (Exception e) {
			return "failure";
		}

	}

	public boolean tokencheck(HttpServletRequest request) {
		final String authHeader = request.getHeader("authorization");
		if (authHeader == null || !authHeader.startsWith("Bearer ")) {
			return false;
		}
		final String token = authHeader.substring(7);
		try {
			final Claims claims = Jwts.parser().setSigningKey("secretkey").parseClaimsJws(token).getBody();
			request.setAttribute("claims", claims);
			return true;
		} catch (final SignatureException e) {
			return false;
		}
	}

	@RequestMapping(value = "/devicebulkupload", method = RequestMethod.POST)
	public @ResponseBody String deviceBulkUpload(final ModelMap model, HttpServletRequest request,
			@RequestParam(value = "file") MultipartFile infile) {
		boolean token_validation = tokencheck(request);
		if (token_validation) {
			try {
				InputStream is = infile.getInputStream();
				BufferedReader br = new BufferedReader(new InputStreamReader(is));
				String line;
				while ((line = br.readLine()) != null) {
					line = line.trim();
					if (!line.isEmpty()) {
						//System.out.println(line);
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			logger.info("File reading complete");
			JobConfiguration jobs = new JobConfiguration(jobService, gatewayService, deviceService, infile, "device"); // Step One
			String Job_Id = jobs.getJobId();
			Thread j = new Thread(jobs);
			j.start();
			return Job_Id;
		} else
			return "Invalid Token";
	}

	@RequestMapping(value = "/gatewaybulkupload", method = RequestMethod.POST)
	public @ResponseBody String gatewayBulkUpload(final ModelMap model, HttpServletRequest request,
			@RequestParam(value = "file") MultipartFile infile) {
		boolean token_validation = tokencheck(request);
		if (token_validation) {
			try {
				InputStream is = infile.getInputStream();
				BufferedReader br = new BufferedReader(new InputStreamReader(is));
				String line;
				while ((line = br.readLine()) != null) {
					line = line.trim();
					if (!line.isEmpty()) {
						//System.out.println(line);
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			logger.info("File reading complete");
			JobConfiguration jobs = new JobConfiguration(jobService, gatewayService, deviceService, infile, "gateway"); // Step One
			String Job_Id = jobs.getJobId();
			Thread j = new Thread(jobs);
			j.start();
			return Job_Id;
		} else
			return "Invalid Token";
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/jobstatus", method = RequestMethod.POST)
	public @ResponseBody String jobStatus(HttpServletRequest request, @RequestParam(value = "jobId") String JobId) {
		boolean token_validation = tokencheck(request);
		if (token_validation) {
			Batch_Job job = jobService.getJobById(JobId);
			String status = job.getJob_Status();
			String remark = job.getJob_Remarks();
			JSONObject resp = new JSONObject();
			resp.put("JobId", JobId.toString());
			resp.put("Status", status.toString());
			resp.put("Remark", remark.toString());
			return resp.toJSONString();
		} else
			return "Invalid Token";
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = { "/registerdevice" }, method = { RequestMethod.POST })
	@ResponseBody
	public String registerdevice(HttpServletRequest request, ModelMap model,
			@RequestParam("device") String devicestring, Principal principal) {
		//System.out.println(devicestring);
		boolean token_validation = tokencheck(request);
		if (token_validation) {
			JSONObject resp = new JSONObject();
			JSONParser jsonParser = new JSONParser();
			JSONObject devicejson = null;
			try {
				devicejson = (JSONObject) jsonParser.parse(devicestring);
				resp.put("parse", Boolean.valueOf(true));
			} catch (ParseException e1) {
				resp.put("parse", Boolean.valueOf(false));
				e1.printStackTrace();
			}
			if (((Boolean) resp.get("parse")).booleanValue()) {
				if (devicejson.containsKey("device_id") && devicejson.containsKey("gateway_id")) {
					JSONObject tag = new JSONObject();
					if (devicejson.containsKey("tag")) {
						tag = (JSONObject) devicejson.get("tag");
						tag.put("gateway_id", devicejson.get("gateway_id").toString());
					} else {
						tag = (JSONObject) (new JSONObject()).put("gateway_id",
								devicejson.get("gateway_id").toString());
					}
					try {
						JSONObject reg = this.deviceManagement
								.deviceRegistration(devicejson.get("device_id").toString(), tag);
						if (reg.containsKey("DeviceId")) {
							resp.put("registered", Boolean.valueOf(true));
							DeviceTwin twinClient=null;
							DeviceTwinDevice devt = null;
							try {
								twinClient = DeviceTwin.createFromConnectionString(
										(new AppProperty()).getPropertyValue("connectionStringIoTHub"));
								devt = new DeviceTwinDevice(reg.get("DeviceId").toString());
								twinClient.getTwin(devt);
							} catch (IOException e) {
								resp.put("registered", Boolean.valueOf(false));
							}
							//System.out.println("Old Device Twin " + devt);
							//String currentTags = devt.tagsToString();
							//System.out.println("Currnet Tags " + currentTags);
							Set<Pair> tags = new HashSet<Pair>();
							for (Object key : tag.keySet()) {
						        String keyStr = key.toString();
						        String keyvalue = tag.get(keyStr).toString();
						       // System.out.println("Objedct"+keyvalue);
						        tags.add(new Pair(keyStr, keyvalue));
							}
							devt.setTags(tags);
							try {
								twinClient.updateTwin(devt);
								twinClient.getTwin(devt);
								//System.out.println("New Device Twin " + devt);
							} catch (IOException e) {
								resp.put("registered", Boolean.valueOf(true));
							}
						} else {
							resp.put("registered", Boolean.valueOf(false));
						}
					} catch (IotHubException e) {
						resp.put("registered", Boolean.valueOf(false));
					}
					if (((Boolean) resp.get("registered")).booleanValue()) {
						DeviceModel device = new DeviceModel();
						device.setGateway_Id(devicejson.get("gateway_id").toString());
						device.setDevice_Id(devicejson.get("device_id").toString());
						device.setReg_Flg(true);
						if (devicejson.containsKey("tag")) {
							device.setTag(((JSONObject) devicejson.get("tag")).toJSONString());
						}
						Date dNow = new Date();
						SimpleDateFormat ft = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss a");
						device.setR_Cre_Time(ft.format(dNow));
						device.setR_Mod_Time(ft.format(dNow));
						this.deviceService.saveOrUpdate(device);
						resp.put("saved", Boolean.valueOf(true));
					} else {
						resp.put("saved", Boolean.valueOf(false));
					}
				} else {
					resp.put("saved", Boolean.valueOf(false));
				}
			} else {
				resp.put("saved", Boolean.valueOf(false));
			}
			return resp.toJSONString();
		} else
			return "Invalid Token";
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = { "/registergateway" }, method = { RequestMethod.POST })
	@ResponseBody
	public String registergateway(HttpServletRequest request, ModelMap model,
			@RequestParam("gateway") String gatewaystring, Principal principal) {
		boolean token_validation = tokencheck(request);
		if (token_validation) {
			JSONObject resp = new JSONObject();
			JSONParser jsonParser = new JSONParser();
			JSONObject gatewayjson = null;
			try {
				gatewayjson = (JSONObject) jsonParser.parse(gatewaystring);
				resp.put("parse", Boolean.valueOf(true));
			} catch (ParseException e1) {
				resp.put("parse", Boolean.valueOf(false));
				e1.printStackTrace();
			}
			if (((Boolean) resp.get("parse")).booleanValue()) {
				if ((gatewayjson.containsKey("gateway_id"))) {
					JSONObject tag = new JSONObject();
					if (gatewayjson.containsKey("tag")) {
						tag = (JSONObject) gatewayjson.get("tag");
					}
					try {
						JSONObject reg = this.deviceManagement
								.deviceRegistration(gatewayjson.get("gateway_id").toString(), tag);
						if (reg.containsKey("DeviceId")) {
							resp.put("registered", Boolean.valueOf(true));
							DeviceTwin twinClient=null;
							DeviceTwinDevice devt = null;
							try {
								twinClient = DeviceTwin.createFromConnectionString(
										(new AppProperty()).getPropertyValue("connectionStringIoTHub"));
								devt = new DeviceTwinDevice(reg.get("DeviceId").toString());
								twinClient.getTwin(devt);
							} catch (IOException e) {
								resp.put("registered", Boolean.valueOf(false));
							}
							//System.out.println("Old Device Twin " + devt);
							//String currentTags = devt.tagsToString();
							//System.out.println("Currnet Tags " + currentTags);
							Set<Pair> tags = new HashSet<Pair>();
							for (Object key : tag.keySet()) {
						        String keyStr = key.toString();
						        String keyvalue = tag.get(keyStr).toString();
						        //System.out.println("Objedct"+keyvalue);
						        tags.add(new Pair(keyStr, keyvalue));
							}
							devt.setTags(tags);
							try {
								twinClient.updateTwin(devt);
								twinClient.getTwin(devt);
								//System.out.println("New Device Twin " + devt);
							} catch (IOException e) {
								resp.put("registered", Boolean.valueOf(true));
							}
						} else {
							resp.put("registered", Boolean.valueOf(false));
						}
					} catch (Exception e) {
						resp.put("registered", Boolean.valueOf(false));
						e.printStackTrace();
					}
					if (((Boolean) resp.get("registered")).booleanValue()) {
						GatewayModel gateway = new GatewayModel();
						gateway.setGateway_Id(gatewayjson.get("gateway_id").toString());
						gateway.setReg_Flg(true);
						if (gatewayjson.containsKey("tag")) {
							gateway.setTag(((JSONObject) gatewayjson.get("tag")).toJSONString());
						}
						Date dNow = new Date();
						SimpleDateFormat ft = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss a");
						gateway.setR_Cre_Time(ft.format(dNow));
						gateway.setR_Mod_Time(ft.format(dNow));
						this.gatewayService.saveOrUpdate(gateway);
						resp.put("saved", Boolean.valueOf(true));
					} else {
						resp.put("saved", Boolean.valueOf(false));
					}
				} else {
					resp.put("saved", Boolean.valueOf(false));
				}
			} else {
				resp.put("saved", Boolean.valueOf(false));
			}
			return resp.toJSONString();
		} else
			return "Invalid Token";
	}
	
	@RequestMapping(value = "/startWebsocket", method = RequestMethod.POST)
	public @ResponseBody String startWebsocket(HttpServletRequest request) throws EventHubException, IOException, InterruptedException, ExecutionException{
		System.out.println("Insd Microservce java appcontroller-/startwebsocket method");
		boolean token_validation = tokencheck(request);
	
		 logger.info("Insd Microservce java appcontroller-/startwebsocket method");
		if (token_validation) {
			messageRead.startClient();
			return "Success";
		} else
			return "Invalid Token";
	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	public @ResponseBody String postInsert(final ModelMap model, @Valid LoginModel jymodel, BindingResult result,
			HttpServletRequest request, @RequestParam("data") String data, UriComponentsBuilder ucBuilder)
			throws IOException, InvalidKeyException, NoSuchAlgorithmException {
		boolean token_validation = tokencheck(request);
		if (token_validation) {
			return "Success";
		} else
			return "Invalid Token";
	}
}
