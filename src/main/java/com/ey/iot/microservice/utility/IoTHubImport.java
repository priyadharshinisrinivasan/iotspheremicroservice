package com.ey.iot.microservice.utility;

import java.security.NoSuchAlgorithmException;
import javax.crypto.KeyGenerator;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.microsoft.azure.sdk.iot.service.AuthenticationMechanism;
import com.microsoft.azure.sdk.iot.service.DeviceStatus;
import com.microsoft.azure.sdk.iot.service.ImportMode;
import com.microsoft.azure.sdk.iot.service.JobProperties;
import com.microsoft.azure.sdk.iot.service.JobProperties.JobStatus;
import com.microsoft.azure.sdk.iot.service.auth.SymmetricKey;
import com.microsoft.azure.sdk.iot.service.RegistryManager;

public class IoTHubImport {
	public String connectionStringIoTHub;
	public RegistryManager registryManager;
	IoTHubImport() {
		this.connectionStringIoTHub = (new AppProperty()).getPropertyValue("connectionStringIoTHub");
	}

	public JobProperties importdev() {
		String URI = (new ContainerManage()).GetContainerSasUri("iotspherecontainer");
		JobProperties importJob;
		try {

			registryManager = RegistryManager.createFromConnectionString(connectionStringIoTHub);
			importJob = registryManager.importDevices(URI, URI);
		} catch (Exception e) {
			importJob = null;
			e.printStackTrace();
		}
		return importJob;
	}

	public int isJobRunning(JobProperties jobProperty) {
		JobProperties job;
		try {
			job = registryManager.getJob(jobProperty.getJobId());
			if (job.getStatus() == JobStatus.COMPLETED || job.getStatus() == JobStatus.CANCELLED
					|| job.getStatus() == JobStatus.FAILED) {
				return 0;
			} else
				return 1;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	@SuppressWarnings("unchecked")
	public JSONObject createSAS(JSONObject dev) {
		SymmetricKey symmetricKey = new SymmetricKey();
		KeyGenerator keyGenerator = null;
		try {
			keyGenerator = KeyGenerator.getInstance("AES");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		symmetricKey.setPrimaryKey(LocalBase64.encodeBase64StringLocal(keyGenerator.generateKey().getEncoded()));
		symmetricKey.setSecondaryKey(LocalBase64.encodeBase64StringLocal(keyGenerator.generateKey().getEncoded()));
		AuthenticationMechanism authentication = new AuthenticationMechanism(symmetricKey);
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();
		try {
			dev.put("authentication", (JSONObject) new JSONParser().parse(gson.toJson(authentication)));
			dev.put("importMode", (ImportMode.CreateOrUpdate).toString());
			if (!dev.containsKey("status"))
				dev.put("status", (DeviceStatus.Enabled).toString());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return dev;
	}
}
