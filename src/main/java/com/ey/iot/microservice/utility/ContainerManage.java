package com.ey.iot.microservice.utility;

import java.security.InvalidKeyException;
import java.util.Calendar;
import java.util.Date;
import java.util.EnumSet;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.BlobContainerPermissions;
import com.microsoft.azure.storage.blob.BlobContainerPublicAccessType;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.SharedAccessBlobPermissions;
import com.microsoft.azure.storage.blob.SharedAccessBlobPolicy;

public class ContainerManage {
	public static String storageConnectionString;

	public ContainerManage() {
		storageConnectionString = (new AppProperty()).getPropertyValue("storageConnectionString");
	}

	public String GetContainerSasUri(String incontainer) {
		String URI;
		CloudBlobContainer container = getContainer(incontainer);
		BlobContainerPermissions permissions = new BlobContainerPermissions();
		permissions.setPublicAccess(BlobContainerPublicAccessType.CONTAINER);
		SharedAccessBlobPolicy sasPolicy = new SharedAccessBlobPolicy();
		sasPolicy.setPermissions(EnumSet.of(SharedAccessBlobPermissions.READ, SharedAccessBlobPermissions.WRITE,
				SharedAccessBlobPermissions.LIST, SharedAccessBlobPermissions.DELETE));
		GregorianCalendar calendar = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
		calendar.setTime(new Date());
		sasPolicy.setSharedAccessStartTime(calendar.getTime());
		calendar.add(Calendar.HOUR, 10);
		sasPolicy.setSharedAccessExpiryTime(calendar.getTime());
		permissions.getSharedAccessPolicies().put("testwasbpolicy", sasPolicy);
		try {
			container.uploadPermissions(permissions);
		} catch (StorageException e1) {
			e1.printStackTrace();
		}
		try {
			URI = container.getUri() + "?" + container.generateSharedAccessSignature(sasPolicy, null);
		} catch (InvalidKeyException | StorageException e) {
			URI = "ERROR";
			e.printStackTrace();
		}
		URI.replace("http://", "https://");
		return URI;
	}

	public CloudBlobContainer createContainer(String containername) {
		CloudStorageAccount storageAccount;
		CloudBlobContainer container;
		try {
			storageAccount = CloudStorageAccount.parse(storageConnectionString);
			CloudBlobClient blobClient = storageAccount.createCloudBlobClient();
			container = blobClient.getContainerReference(containername);
			container.createIfNotExists();
			BlobContainerPermissions containerPermissions = new BlobContainerPermissions();
			containerPermissions.setPublicAccess(BlobContainerPublicAccessType.CONTAINER);
			container.uploadPermissions(containerPermissions);
		} catch (Exception e) {
			container = null;
			e.printStackTrace();
		}
		return container;
	}

	public CloudBlobContainer getContainer(String containername) {
		CloudStorageAccount storageAccount;
		CloudBlobContainer container;
		try {
			storageAccount = CloudStorageAccount.parse(storageConnectionString);
			CloudBlobClient blobClient = storageAccount.createCloudBlobClient();
			container = blobClient.getContainerReference(containername);
		} catch (Exception e) {
			container = null;
			e.printStackTrace();
		}
		return container;
	}
}