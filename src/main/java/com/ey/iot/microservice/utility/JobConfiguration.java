package com.ey.iot.microservice.utility;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.web.multipart.MultipartFile;

import com.ey.iot.microservice.model.Batch_Job;
import com.ey.iot.microservice.model.DeviceModel;
import com.ey.iot.microservice.model.GatewayModel;
import com.ey.iot.microservice.service.Batch_JobService;
import com.ey.iot.microservice.service.DeviceService;
import com.ey.iot.microservice.service.GatewayService;
import com.ey.iot.microservice.utility.ContainerManage;
import com.microsoft.azure.sdk.iot.service.JobProperties;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlockBlob;

public class JobConfiguration implements Runnable {
	private GatewayService gatewayService;
	private DeviceService deviceService;
	private Batch_JobService jobService;
	private String jobId;
	private MultipartFile file;
	private List<JSONObject> devices;
	private String devicesStr;
	private List<JSONObject> gateways;
	private String gatewaysStr;
	private Batch_Job job;
	private String CurrentTime;
	private String type;
	private IoTHubImport iothub;

	public JobConfiguration(Batch_JobService jobService, GatewayService gatewayService, DeviceService deviceService,
			MultipartFile infile, String type) {
		this.setType(type);
		this.jobService = jobService;
		createJob(type);
		this.gatewayService = gatewayService;
		this.deviceService = deviceService;
		this.file = infile;
		this.iothub = new IoTHubImport();
	}

	public void createJob(String type) {
		Batch_Job job = new Batch_Job();
		if (type == "device") {
			job.setJob_Type("BulK_Upload");
			job.setJob_Desc("Device Bulk Upload");
		} else {
			job.setJob_Type("BulK_Upload");
			job.setJob_Desc("Gateway Bulk Upload");
		}
		job.setJob_Id("Job-" + new Date().getTime());
		job.setR_Cre_Time(getCurrentTime());
		job.setR_Mod_Time(getCurrentTime());
		job.setJob_Start_Time(getCurrentTime());
		job.setJob_Status("A");
		job.setLogin_User("loginid");
		job.setJob_Remarks("INFO: Job Available");
		jobService.addJob(job);
		this.setJobId(job.getJob_Id());
		this.setJob(job);
	}

	public boolean deviceValidate() {
		BufferedReader br;
		setDevicesStr("");
		boolean error = true;
		String msg;
		JSONParser jsonParser = new JSONParser();
		setDevices(new ArrayList<JSONObject>());
		try {
			String line;
			/*FileReader is = new FileReader(file.getPath());
			br = new BufferedReader(is);*/
			InputStream is = file.getInputStream();
			br = new BufferedReader(new InputStreamReader(is));
			int i = 1;
			while ((line = br.readLine()) != null) {
				line = line.trim();
				if (!line.isEmpty()) {
					JSONObject temp;
					try {
						temp = (JSONObject) jsonParser.parse(line);
						if (jsonChecking(temp)) {
							JSONObject tags = (JSONObject) temp.get("tags");
							if (gatewayService.isGatewayExist(tags.get("gateway").toString())) {
								temp = iothub.createSAS(temp);
								getDevices().add(temp);
								setDevicesStr(getDevicesStr() + temp.toJSONString() + "\n");
								Batch_Job job = jobService.getJobById(getJobId());
								job.setJob_Status("VP");
								job.setJob_Remarks("INFO: Validation Processing");
								jobService.updateJob(job);
								error = false;
							} else {
								msg = "ERROR: Gateway not Found. Line number" + i;
								Batch_Job job = jobService.getJobById(getJobId());
								job.setJob_Status("VE");
								job.setJob_Remarks(msg);
								jobService.updateJob(job);
								error = true;
								break;
							}
						} else {
							msg = "ERROR: Details missing. Line number" + i;
							Batch_Job job = jobService.getJobById(getJobId());
							job.setJob_Status("VE");
							job.setJob_Remarks(msg);
							jobService.updateJob(job);
							error = true;
							break;
						}
					} catch (ParseException e) {
						error = true;
						msg = "ERROR: Wrong JSON format. Line number" + i;
						Batch_Job job = jobService.getJobById(getJobId());
						job.setJob_Status("VE");
						job.setJob_Remarks(msg);
						jobService.updateJob(job);
						break;
					}
				}
				i++;
			}
		} catch (IOException e) {
			msg = "ERROR: Unable to read File";
			Batch_Job job = jobService.getJobById(getJobId());
			job.setJob_Status("VE");
			job.setJob_Remarks(msg);
			jobService.updateJob(job);
			error = true;
		}
		return error;
	}

	private boolean jsonChecking(JSONObject temp) {
		boolean status = false;
		if (temp.containsKey("id")) {
			if(temp.containsKey("tags")) {
				JSONObject gw = (JSONObject) temp.get("tags");
				if(gw.containsKey("gateway"))
					status = true;
			}
		}
		return status;
	}

	public boolean uploadDevice() {
		boolean status = true;
		if (getDevices() != null) {
			CloudBlockBlob blob;
			ContainerManage cm = new ContainerManage();
			CloudBlobContainer container = cm.createContainer("iotspherecontainer");
			if (container != null && status) {
				try {
					blob = container.getBlockBlobReference("devices.txt");
					blob.upload(file.getInputStream(), file.getSize());
				} catch (URISyntaxException | StorageException | IOException e) {
					status = false;
				}
			} else
				status = false;
		} else
			status = false;
		return status;

	}

	public boolean uploadGateway() {
		boolean status = true;
		if (getGateways() != null) {
			CloudBlockBlob blob;
			ContainerManage cm = new ContainerManage();
			CloudBlobContainer container = cm.createContainer("iotspherecontainer");
			if (container != null && status) {
				try {
					blob = container.getBlockBlobReference("devices.txt");
					blob.upload(this.file.getInputStream(), this.file.getSize());
				} catch (URISyntaxException | StorageException | IOException e) {
					status = false;
				}
			} else
				status = false;
		} else
			status = false;
		return status;

	}

	public boolean register_IoTHub() {
		boolean status;
		if (type == "device")
			status = uploadDevice();
		else
			status = uploadGateway();
		if (status) {
			Batch_Job job = jobService.getJobById(getJobId());
			job.setJob_Status("FUP");
			job.setJob_Remarks("File Upload Processing");
			jobService.updateJob(job);
			JobProperties jobProperty = iothub.importdev();
			while (true) {
				if (iothub.isJobRunning(jobProperty) == 1) {
					job.setJob_Status("DRP");
					job.setJob_Remarks("INFO: Device Registration Processing");
					jobService.updateJob(job);
				} else if(iothub.isJobRunning(jobProperty) == -1){ 
					job.setJob_Status("IE");
					job.setJob_Remarks("ERROR: IoT Hub Import Error");
					jobService.updateJob(job);
					status = false;
					break;
				}else {
					job.setJob_Status("DRC");
					job.setJob_Remarks("INFO: Device Registration Complete");
					jobService.updateJob(job);
					break;
				}
			}
		} else {
			Batch_Job job = jobService.getJobById(getJobId());
			job.setJob_Status("VE");
			job.setJob_Remarks("Error: Upload to blob.");
			jobService.updateJob(job);
		}
		return status;
	}

	public void saveInDatabase_device() {
		if (getDevices() != null) {
			for (JSONObject device : getDevices()) {
				DeviceModel dev = new DeviceModel();
				//if (!deviceService.isDeviceExists(device.get("id").toString())) {
					JSONObject tags = (JSONObject) device.get("tags");
					dev.setDevice_Id(device.get("id").toString());
					dev.setGateway_Id(tags.get("gateway").toString());
					dev.setTag(tags.toJSONString());
					dev.setR_Cre_Time(getCurrentTime());
					dev.setR_Mod_Time(getCurrentTime());
					dev.setReg_Flg(true);
					deviceService.saveOrUpdate(dev);
					Batch_Job job = jobService.getJobById(getJobId());
					job.setJob_Status("DBC");
					job.setJob_Remarks("INFO: Database saved");
					jobService.updateJob(job);
			//	}
			}
		}
	}

	@Override
	public void run() {
		if (type == "device") {
			boolean status = deviceValidate(); // Step 2
			if (!status) {
				status = register_IoTHub(); // Step 3
				if (status) {
					saveInDatabase_device(); // Step 4
					Batch_Job job = jobService.getJobById(getJobId());
					job.setJob_Status("PC");
					job.setJob_Remarks("INFO: Processing done.");
					job.setR_Mod_Time(getCurrentTime());
					job.setJob_End_Time(getCurrentTime());
					jobService.updateJob(job);
				}
			}
		} else {
			boolean status = gatewayValidation();
			if (!status) {
				status = register_IoTHub(); // Step 3
				if (status) {
					saveInDatabase_Gateway(); // Step 4
					Batch_Job job = jobService.getJobById(getJobId());
					job.setJob_Status("PC");
					job.setJob_Remarks("INFO: Processing done.");
					job.setR_Mod_Time(getCurrentTime());
					job.setJob_End_Time(getCurrentTime());
					jobService.updateJob(job);
				}
			}
		}
	}

	private void saveInDatabase_Gateway() {
		if (getGateways() != null) {
			for (JSONObject device : getGateways()) {
				GatewayModel dev = new GatewayModel();
				if (!gatewayService.isGatewayExist(device.get("id").toString())) {
					dev.setGateway_Id(device.get("id").toString());
					//dev.setR_Cre_Time(getCurrentTime());
					dev.setR_Mod_Time(getCurrentTime());
					dev.setReg_Flg(true);
					gatewayService.saveOrUpdate(dev);
					Batch_Job job = jobService.getJobById(getJobId());
					job.setJob_Status("DBC");
					job.setJob_Remarks("INFO: Database saved");
					jobService.updateJob(job);
				}
			}
		}
	}

	private boolean gatewayValidation() {
		BufferedReader br;
		boolean error = true;
		String msg;
		JSONParser jsonParser = new JSONParser();
		setGateways(new ArrayList<JSONObject>());
		try {
			String line;
			/*FileReader is = new FileReader(file.getPath());
			br = new BufferedReader(is);*/
			InputStream is = file.getInputStream();
			br = new BufferedReader(new InputStreamReader(is));
			int i = 1;
			setGatewaysStr("");
			while ((line = br.readLine()) != null) {
				line = line.trim();
				if (!line.isEmpty()) {
					JSONObject temp;
					try {
						temp = (JSONObject) jsonParser.parse(line);
						if (jsonCheckingForGateway(temp)) {
							temp = iothub.createSAS(temp);
							getGateways().add(temp);
							setGatewaysStr(getGatewaysStr() + temp.toJSONString() + "\n");
							Batch_Job job = jobService.getJobById(getJobId());
							job.setJob_Status("VP");
							job.setJob_Remarks("INFO: Validation Processing");
							jobService.updateJob(job);
							error = false;
						} else {
							msg = "ERROR: Gateway not Found. Line number" + i;
							Batch_Job job = jobService.getJobById(getJobId());
							job.setJob_Status("VE");
							job.setJob_Remarks(msg);
							jobService.updateJob(job);
							error = true;
							break;
						}
					} catch (ParseException e) {
						error = true;
						msg = "ERROR: Wrong JSON format. Line number" + i;
						Batch_Job job = jobService.getJobById(getJobId());
						job.setJob_Status("VE");
						job.setJob_Remarks(msg);
						jobService.updateJob(job);
						break;
					}
				}
				i++;
			}
		} catch (IOException e) {
			msg = "ERROR: Unable to read File";
			Batch_Job job = jobService.getJobById(getJobId());
			job.setJob_Status("VE");
			job.setJob_Remarks(msg);
			jobService.updateJob(job);
			error = true;
		}
		return error;
	}

	private boolean jsonCheckingForGateway(JSONObject temp) {
		boolean status = false;
		if (temp.containsKey("id")) {
			status = true;
		}
		return status;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public List<JSONObject> getDevices() {
		return devices;
	}

	public void setDevices(List<JSONObject> devices) {
		this.devices = devices;
	}

	public Batch_Job getJob() {
		return job;
	}

	public void setJob(Batch_Job job) {
		this.job = job;
	}

	public String getCurrentTime() {
		this.setCurrentTime(new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a").format(new Date()));
		return CurrentTime;
	}

	public void setCurrentTime(String currentTime) {
		CurrentTime = currentTime;
	}

	public String getDevicesStr() {
		return devicesStr;
	}

	public void setDevicesStr(String devicesStr) {
		this.devicesStr = devicesStr;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<JSONObject> getGateways() {
		return gateways;
	}

	public void setGateways(List<JSONObject> gateways) {
		this.gateways = gateways;
	}

	public String getGatewaysStr() {
		return gatewaysStr;
	}

	public void setGatewaysStr(String gatewaysStr) {
		this.gatewaysStr = gatewaysStr;
	}

}
